#!/bin/bash
f=$(pwd)
# arquivo="${f}/${1}"
arquivo="../check.png"

# convert -resize 512x512 "${f}/${1}" --out "${f}/iTune512"
# convert -resize 1024x1024 "${f}/${1}" --out "${f}/iTune1024@2x"

convert -resize 60x60 $arquivo  "${f}/icon-60.png"
convert -resize 120x120 $arquivo  "${f}/icon-60@2x.png"
convert -resize 180x180 $arquivo  "${f}/icon-60@3x.png"

convert -resize 76x76 $arquivo  "${f}/icon-76.png"
convert -resize 152x152 $arquivo  "${f}/icon-76@2x.png"

convert -resize 40x40 $arquivo  "${f}/icon-40.png"
convert -resize 80x80 $arquivo  "${f}/icon-40@2x.png"

convert -resize 57x57 $arquivo  "${f}/icon.png"
convert -resize 114x114 $arquivo  "${f}/icon@2x.png"

convert -resize 72x72 $arquivo  "${f}/icon-72.png"
convert -resize 144x144 $arquivo  "${f}/icon-72@2x.png"

convert -resize 29x29 $arquivo  "${f}/icon-small.png"
convert -resize 58x58 $arquivo  "${f}/icon-small@2x.png"

convert -resize 50x50 $arquivo  "${f}/icon-50.png"
convert -resize 100x100 $arquivo  "${f}/icon-50@2x.png"
