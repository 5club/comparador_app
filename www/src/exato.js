angular.module('app', ['ngCookies','ui.utils.masks']);



function ExatoCtrl($scope,$cookieStore) {
    
    
    
    $scope.itens = [
        {unidade: '', undValor: '', resultado: '' ,cor: '', corFundo: '', todo: ''},
        {unidade: '', undValor: '', resultado: '' ,cor: '', corFundo: '', todo: ''},
        {unidade: '', undValor: '', resultado: '' ,cor: '', corFundo: '', todo: ''}
    ];
    
    
    var itensCookie = $cookieStore.get('itensCookie');
   
    if( typeof itensCookie != "undefined"){
        $scope.itens = itensCookie;                
     }
    
        
    $scope.salvarCookie = function() {
      $cookieStore.put('itensCookie',$scope.itens);
    };
    
     $scope.addItem = function() {
               
                $scope.itens.push({unidade: '', undValor: '', cor: '', corFundo: ''});

    };
    
    $scope.mostrar = function(item) {
        
        if(item.unidade!=0)
        {
            $scope.ehMenor(item);
            
            if(item.resultado!=''){
                return true;
            }else{
                return false;
            }
        }
        
        return false;
        
    };
    
    
    $scope.limpar = function(){
        
        $scope.itens = [
            {unidade: '', undValor: '', resultado: '' ,cor: '', corFundo: '', todo: ''},
            {unidade: '', undValor: '', resultado: '' ,cor: '', corFundo: '', todo: ''},
            {unidade: '', undValor: '', resultado: '' ,cor: '', corFundo: '', todo: ''}
        ];
        
        $scope.salvarCookie();
                
    };
    
    $scope.ehMenor = function(item) {

        $scope.salvarCookie();
        
        // ja comeca de verde
        item.cor = '#5b7804';
        item.corFundo = '#e1f0b8';
//        item.todo = '#e1f0b8';
        
        var rsUnd = item.undValor/item.unidade;

        item.resultado = rsUnd;
        
//        console.log(item.unidade+'-->>>>'+rsUnd);
//        
//        if(rsUnd!=0){
//            console.log('entrou');
//            item.resultado = rsUnd;
//        }
        
        var lenList = $scope.itens.length;
        
        
        for(var i=0; i<lenList; i++){
            
            
            var rsUndIt = $scope.itens[i].undValor/$scope.itens[i].unidade;
            
            if(isNaN(rsUndIt) || rsUndIt==0){
                
                $scope.itens[i].cor = '#ffffff';
                $scope.itens[i].corFundo = '#ffffff';
//                $scope.itens[i].todo = '#ffffff';
                
            }
            
            if( rsUnd>rsUndIt && rsUndIt!=0 ){
                
                item.cor = '#990f01';
                item.corFundo = '#f6e3e1';
                item.todo = '#ffff';
//                item.todo = '#f6e3e1';
            }
            
            
        }
        
        
    };
    
}
